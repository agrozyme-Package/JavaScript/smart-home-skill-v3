import Endpoint from '@agrozyme/alexa-skill-type/source/SmartHome/Type/Endpoint';
import Header from '@agrozyme/alexa-skill-type/source/SmartHome/Type/Header';

export default abstract class Base {
  protected endpoint: Endpoint;
  protected header: Header;

  constructor(header: Header, endpoint: Endpoint) {
    this.header = JSON.parse(JSON.stringify(header));
    this.endpoint = endpoint;
    this.setup();
  }

  protected setup() {
  }
}
