import {BrightnessControllerPropertyName} from '@agrozyme/alexa-skill-type/source/SmartHome/Enumeration/Capability/BrightnessController';
import {PowerControllerPropertyName} from '@agrozyme/alexa-skill-type/source/SmartHome/Enumeration/Capability/PowerController';
import {ThermostatControllerPropertyName} from '@agrozyme/alexa-skill-type/source/SmartHome/Enumeration/Capability/ThermostatController';
import SkillNamespace from '@agrozyme/alexa-skill-type/source/SmartHome/Enumeration/SkillNamespace';
import {CapabilityItem as CapabilityItem} from '@agrozyme/alexa-skill-type/source/SmartHome/Type/Capability/Discovery';

class Capability {
  static readonly instance: Capability = new Capability();
  protected items: { [index: string]: string[] };

  constructor() {
    this.items = this.makeItems();
  }

  // noinspection JSUnusedGlobalSymbols
  getItem(index: SkillNamespace, proactivelyReported: boolean, retrievable: boolean): CapabilityItem {
    const item: CapabilityItem = {
      type: 'AlexaInterface',
      interface: index,
      version: '3'
    };
    const supported = this.items.hasOwnProperty(index) ? this.items[index].map(item => ({name: item})) : [];

    if (0 < supported.length) {
      item.properties = {
        supported,
        proactivelyReported,
        retrievable
      };
    }

    return item;
  }

  // noinspection JSMethodCanBeStatic
  protected makeItems() {
    const items: { [index: string]: string[] } = {};
    items[SkillNamespace.PowerController] = [PowerControllerPropertyName.powerState];
    items[SkillNamespace.BrightnessController] = [BrightnessControllerPropertyName.brightness];
    items[SkillNamespace.ThermostatController] = [ThermostatControllerPropertyName.targetSetpoint, ThermostatControllerPropertyName.lowerSetpoint, ThermostatControllerPropertyName.upperSetpoint, ThermostatControllerPropertyName.thermostatMode];
    return items;
  }
}

const instance = Capability.instance;

// noinspection JSUnusedGlobalSymbols
export default instance;
