import CookingError from '@agrozyme/alexa-skill-type/source/SmartHome/Enumeration/Error/Cooking';
import SkillNamespace from '@agrozyme/alexa-skill-type/source/SmartHome/Enumeration/SkillNamespace';
import SkillRequest from '@agrozyme/alexa-skill-type/source/SmartHome/Type/SkillRequest';
import Base from '../Extend/Base/Error';

export default class Cooking extends Base {

  constructor(event: SkillRequest) {
    super(event);
    this.header.namespace = SkillNamespace.Cooking;
  }

  getCookDurationTooLong(message: string, maxCookTime: string) {
    const payload = {
      type: CookingError.COOK_DURATION_TOO_LONG,
      message,
      maxCookTime
    };
    return this.makeError(payload);
  }

  getDoorClosedTooLong(message: string) {
    const payload = {
      type: CookingError.DOOR_CLOSED_TOO_LONG,
      message
    };
    return this.makeError(payload);
  }

  getDoorOpen(message: string) {
    const payload = {
      type: CookingError.DOOR_OPEN,
      message
    };
    return this.makeError(payload);
  }

  getRemoteStartDisabled(message: string) {
    const payload = {
      type: CookingError.REMOTE_START_DISABLED,
      message
    };
    return this.makeError(payload);
  }

  getRemoteStartNotSupported(message: string) {
    const payload = {
      type: CookingError.REMOTE_START_NOT_SUPPORTED,
      message
    };
    return this.makeError(payload);
  }

}
