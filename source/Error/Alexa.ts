import AlexaError from '@agrozyme/alexa-skill-type/source/SmartHome/Enumeration/Error/Alexa';
import SkillNamespace from '@agrozyme/alexa-skill-type/source/SmartHome/Enumeration/SkillNamespace';
import SkillRequest from '@agrozyme/alexa-skill-type/source/SmartHome/Type/SkillRequest';
import Base from '../Extend/Base/Error';

export interface TemperatureValue {
  scale: string;
  value: number;
}

export default class Alexa extends Base {

  constructor(event: SkillRequest) {
    super(event);
    this.header.namespace = SkillNamespace.Alexa;
  }

  getBridgeUnreachable(message: string) {
    const payload = {
      type: AlexaError.BRIDGE_UNREACHABLE,
      message
    };
    return this.makeError(payload);
  }

  getEndpointBusy(message: string) {
    const payload = {
      type: AlexaError.ENDPOINT_BUSY,
      message
    };
    return this.makeError(payload);
  }

  getEndpointLowPower(message: string) {
    const payload = {
      type: AlexaError.ENDPOINT_LOW_POWER,
      message
    };
    return this.makeError(payload);
  }

  getEndpointUnreachable(message: string) {
    const payload = {
      type: AlexaError.ENDPOINT_UNREACHABLE,
      message
    };
    return this.makeError(payload);
  }

  getExpiredAuthorizationCredential(message: string) {
    const payload = {
      type: AlexaError.EXPIRED_AUTHORIZATION_CREDENTIAL,
      message
    };
    return this.makeError(payload);
  }

  getFirmwareOutOfDate(message: string) {
    const payload = {
      type: AlexaError.FIRMWARE_OUT_OF_DATE,
      message
    };
    return this.makeError(payload);
  }

  getHardwareMalfunction(message: string) {
    const payload = {
      type: AlexaError.HARDWARE_MALFUNCTION,
      message
    };
    return this.makeError(payload);
  }

  getInternalError(message: string) {
    const payload = {
      type: AlexaError.INTERNAL_ERROR,
      message
    };
    return this.makeError(payload);
  }

  getInvalidAuthorizationCredential(message: string) {
    const payload = {
      type: AlexaError.INVALID_AUTHORIZATION_CREDENTIAL,
      message
    };
    return this.makeError(payload);
  }

  getInvalidDirective(message: string) {
    const payload = {
      type: AlexaError.INVALID_DIRECTIVE,
      message
    };
    return this.makeError(payload);
  }

  getInvalidValue(message: string) {
    const payload = {
      type: AlexaError.INVALID_VALUE,
      message
    };
    return this.makeError(payload);
  }

  getNoSuchEndpoint(message: string) {
    const payload = {
      type: AlexaError.NO_SUCH_ENDPOINT,
      message
    };
    return this.makeError(payload);
  }

  getNotSuppoeredInCurrentMode(message: string) {
    const payload = {
      type: AlexaError.NOT_SUPPORTED_IN_CURRENT_MODE,
      message
    };
    return this.makeError(payload);
  }

  getRateLimitExceeded(message: string) {
    const payload = {
      type: AlexaError.RATE_LIMIT_EXCEEDED,
      message
    };
    return this.makeError(payload);
  }

  getTemperatureValueOutOfRange(message: string, minimumValue: TemperatureValue, maximumValue: TemperatureValue) {
    const payload = {
      type: AlexaError.TEMPERATURE_VALUE_OUT_OF_RANGE,
      message,
      validRange: {
        minimumValue,
        maximumValue
      }
    };
    return this.makeError(payload);
  }

  getValueOutOfRange(message: string, minimumValue: number, maximumValue: number) {
    const payload = {
      type: AlexaError.VALUE_OUT_OF_RANGE,
      message,
      validRange: {
        minimumValue,
        maximumValue
      }
    };
    return this.makeError(payload);
  }

}
