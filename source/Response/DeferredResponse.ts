import ResponseName from '@agrozyme/alexa-skill-type/source/SmartHome/Enumeration/ResponseName';
import SkillNamespace from '@agrozyme/alexa-skill-type/source/SmartHome/Enumeration/SkillNamespace';
import Base from './Base';

// noinspection JSUnusedGlobalSymbols
export default class Deferred extends Base {

  run(estimatedDeferralInSeconds: number = 8) {
    return {
      event: {
        header: this.header,
        payload: {estimatedDeferralInSeconds}
      }
    };
  }

  protected setup() {
    super.setup();
    this.header.namespace = SkillNamespace.Alexa;
    this.header.name = ResponseName.Deferred;
  }
}
