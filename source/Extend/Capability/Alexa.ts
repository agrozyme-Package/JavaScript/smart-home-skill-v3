import Name from '@agrozyme/alexa-skill-type/source/SmartHome/Enumeration/Capability/Alexa';
import ResponseName from '@agrozyme/alexa-skill-type/source/SmartHome/Enumeration/ResponseName';
import Property from '@agrozyme/alexa-skill-type/source/SmartHome/Type/Property';
import SkillResponse from '@agrozyme/alexa-skill-type/source/SmartHome/Type/SkillResponse';
import Capability, {Actions} from '../Base/Capability';

// noinspection JSUnusedGlobalSymbols
export default abstract class Alexa extends Capability {
  // noinspection JSMethodCanBeStatic
  async doReportState(): Promise<Property[]> {
    return [];
  }

  async run(): Promise<SkillResponse> {
    const properties = await this.dispatch();
    const header = this.makeHeader(ResponseName.StateReport);
    const endpoint = this.skill.getEndpoint();
    return {
      context: {properties: properties},
      event: {
        header,
        endpoint,
        payload: {}
      }
    };
  }

  protected makeActions(): Actions {
    const items = super.makeActions();
    items[Name.ReportState] = this.doReportState;
    return items;
  }

}

