import {PowerControllerRequestName} from '@agrozyme/alexa-skill-type/source/SmartHome/Enumeration/Capability/PowerController';
import ResponseName from '@agrozyme/alexa-skill-type/source/SmartHome/Enumeration/ResponseName';
import {PowerControllerReponse} from '@agrozyme/alexa-skill-type/source/SmartHome/Type/Capability/PowerController';
import Property from '@agrozyme/alexa-skill-type/source/SmartHome/Type/Property';
import Capability, {Actions} from '../Base/Capability';

// noinspection JSUnusedGlobalSymbols
export default abstract class PowerController extends Capability {
  async run(): Promise<PowerControllerReponse> {
    const properties = await this.dispatch();
    const endpoint = this.skill.getEndpoint();
    const header = this.makeHeader(ResponseName.StateReport);
    return {
      context: {properties},
      event: {
        header,
        endpoint,
        payload: {}
      }
    };
  }

  protected async doTurnOff(): Promise<Property[]> {
    throw this.skill.error.alexa.getInternalError('Not Support');
  }

  protected async doTurnOn(): Promise<Property[]> {
    throw this.skill.error.alexa.getInternalError('Not Support');
  }

  protected makeActions(): Actions {
    const items = super.makeActions();
    items[PowerControllerRequestName.TurnOn] = this.doTurnOn;
    items[PowerControllerRequestName.TurnOff] = this.doTurnOff;
    return items;
  }

}
