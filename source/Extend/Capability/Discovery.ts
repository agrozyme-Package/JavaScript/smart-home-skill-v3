import {
  DiscoveryEndpoint, DiscoveryResponse
} from '@agrozyme/alexa-skill-type/source/SmartHome/Type/Capability/Discovery';
import Capability from '../Base/Capability';

// noinspection JSUnusedGlobalSymbols
export default abstract class Discovery extends Capability {
  // noinspection JSMethodCanBeStatic
  async getDiscoveryEndpoints(): Promise<DiscoveryEndpoint[]> {
    return [];
  }

  async run(): Promise<DiscoveryResponse> {
    const item = this.skill.makeEmptyDiscoveryResponse();
    item.event.payload.endpoints = await this.getDiscoveryEndpoints();
    return item;
  }

}
