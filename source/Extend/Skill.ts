import ResponseName from '@agrozyme/alexa-skill-type/source/SmartHome/Enumeration/ResponseName';
import SkillNamespace from '@agrozyme/alexa-skill-type/source/SmartHome/Enumeration/SkillNamespace';
import {DiscoveryResponse} from '@agrozyme/alexa-skill-type/source/SmartHome/Type/Capability/Discovery';
import Header from '@agrozyme/alexa-skill-type/source/SmartHome/Type/Header';
import SkillRequest from '@agrozyme/alexa-skill-type/source/SmartHome/Type/SkillRequest';
import {Callback, Context} from 'aws-lambda';
import AlexaError from '../Error/index';
import Base from './Base/Base';
import Capability, {CapabilityConstructor} from './Base/Capability';
import Provider from './Provider';

export default abstract class Skill extends Base {
  readonly callback: Callback;
  readonly context: Context;
  readonly error: AlexaError;
  readonly event: SkillRequest;
  protected cacheCapabilities: { [index: string]: Capability } = {};
  protected capabilities: { [index: string]: CapabilityConstructor } = {};

  // noinspection TypeScriptAbstractClassConstructorCanBeMadeProtected
  constructor(event: SkillRequest, context: Context, callback: Callback) {
    super();
    this.event = event;
    this.context = context;
    this.callback = callback;
    this.error = new AlexaError(event);
    this.capabilities = this.makeCapabilities();
  }

  getCapability(index: string): Capability {
    const cache = this.cacheCapabilities;

    if (cache.hasOwnProperty(index)) {
      return cache[index];
    }

    const items = this.capabilities;

    if (false === items.hasOwnProperty(index)) {
      return new Capability(this);
    }

    const classConstructor = items[index];
    const item = new classConstructor(this);
    cache[index] = item;
    return item;
  }

  getEndpoint() {
    const item = this.event.directive.endpoint;

    if (undefined === item) {
      throw this.error.alexa.getInvalidDirective('No Endpoint');
    }

    return item;
  }

  getNamespace() {
    return this.event.directive.header.namespace;
  }

  abstract getProvider(): Provider;

  // noinspection JSUnusedGlobalSymbols
  getScope() {
    const item = this.isDiscovery() ? this.event.directive.payload.scope : this.getEndpoint().scope;

    if (undefined === item) {
      throw this.error.alexa.getInvalidDirective('No Scope');
    }

    return item;
  }

  makeEmptyDiscoveryResponse(): DiscoveryResponse {
    const header: Header = this.clone(this.event.directive.header);
    header.namespace = SkillNamespace.Discovery;
    header.name = ResponseName.Discover;
    return {
      event: {
        header,
        payload: {endpoints: []}
      }
    };
  }

  async run(): Promise<any> {
    try {
      this.debug('Request', this.event, {depth: null});
      const capability = this.getCapability(this.getNamespace());
      const response = await capability.run();
      this.debug('Response', response, {depth: null});
      this.callback(null, response);
    } catch (error) {
      this.errorHandler(error);
    }
  }

  protected errorHandler(error: any) {
    if (error instanceof Error) {
      this.debug('Error', error.message);
    } else {
      this.debug('Error', error);
    }

    if (this.isDiscovery()) {
      this.callback(null, this.makeEmptyDiscoveryResponse());
    } else {
      this.callback(error);
    }
  }

  protected isDiscovery() {
    return SkillNamespace.Discovery === this.getNamespace();
  }

  // noinspection JSMethodCanBeStatic
  protected makeCapabilities(): { [index: string]: CapabilityConstructor } {
    return {};
  }

  getValue(index: string): any {
    const payload = this.event.directive.payload;
    return (<object>payload).hasOwnProperty(index) ? payload[index] : undefined;
  }

  getValues(names: string[]): { [index: string]: any } {
    const payload: any = this.event.directive.payload;
    const items: { [index: string]: any } = {};

    names.forEach(name => {
      if (payload.hasOwnProperty(name)) {
        items[name] = payload[name];
      }
    });

    return items;
  }

}
