import {BrightnessControllerPropertyName} from '@agrozyme/alexa-skill-type/source/SmartHome/Enumeration/Capability/BrightnessController';
import {PowerControllerPropertyName} from '@agrozyme/alexa-skill-type/source/SmartHome/Enumeration/Capability/PowerController';
import {TemperatureSensorPropertyName} from '@agrozyme/alexa-skill-type/source/SmartHome/Enumeration/Capability/TemperatureSensor';
import {ThermostatControllerPropertyName} from '@agrozyme/alexa-skill-type/source/SmartHome/Enumeration/Capability/ThermostatController';
import SkillNamespace from '@agrozyme/alexa-skill-type/source/SmartHome/Enumeration/SkillNamespace';
import Header, {HeaderName} from '@agrozyme/alexa-skill-type/source/SmartHome/Type/Header';
import Property from '@agrozyme/alexa-skill-type/source/SmartHome/Type/Property';
import SkillResponse from '@agrozyme/alexa-skill-type/source/SmartHome/Type/SkillResponse';
import AlexaError from '../../Error/Alexa';
import Skill from '../Skill';
import Base from './Base';

export type CapabilityConstructor = { new(skill: Skill): Capability };

export type PropertyName =
  TemperatureSensorPropertyName
  | ThermostatControllerPropertyName
  | PowerControllerPropertyName
  | BrightnessControllerPropertyName;

export interface Actions {
  [index: string]: () => Promise<any>
}

export default class Capability extends Base {
  readonly skill: Skill;
  protected actions: Actions = {};

  constructor(skill: Skill) {
    super();
    this.skill = skill;
    this.actions = this.makeActions();
  }

  // noinspection JSMethodCanBeStatic,JSUnusedGlobalSymbols,JSUnusedLocalSymbols
  getProperties(items: any): Property[] {
    return [];
  }

  async run(): Promise<SkillResponse> {
    throw new AlexaError(this.skill.event).getInternalError('Not Support');
  }

  protected async dispatch() {
    const event = this.skill.event;
    const name = event.directive.header.name;
    const actions = this.actions;

    if (actions.hasOwnProperty(name)) {
      return await actions[name].call(this);
    }

    throw new AlexaError(event).getInvalidDirective('Not Supported');
  }

  // noinspection JSMethodCanBeStatic
  protected emptyProperty(): Property {
    return {
      name: '',
      namespace: '',
      timeOfSample: '',
      uncertaintyInMilliseconds: 0
    };
  }

  // noinspection JSMethodCanBeStatic
  protected filterProperty(item: Property): boolean {
    return ('' !== item.name) && ('' !== item.namespace) && ('' !== item.timeOfSample);
  }

  // noinspection JSMethodCanBeStatic
  protected makeActions(): Actions {
    return {};
  }

  protected makeHeader(name: HeaderName) {
    const item: Header = this.clone(this.skill.event.directive.header);
    item.name = name;
    return item;
  }

  // noinspection JSMethodCanBeStatic
  protected makeProperties(): Property[] {
    return [];
  }

  // noinspection JSMethodCanBeStatic
  protected makeProperty(namespace: SkillNamespace, name: PropertyName, value?: any): Property {
    const now = new Date();
    const item: any = {
      namespace,
      name,
      timeOfSample: now.toISOString(),
      uncertaintyInMilliseconds: now.getUTCMilliseconds()
    };

    if (undefined !== value) {
      item.value = value;
    }

    return item;
  }

}
